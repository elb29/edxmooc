from .archi_models import *

"""leNet5 : CNN architecture LENET 5
Goal : Classification
Input of the NN : image with 1 channel (28x28)
Output of the NN : vector (1xnumber of classes) with each digit of the vector corresponding to a probability of matching which each class


mean results on MNIST : env 97% 
"""

class leNet5(architecture):
    def __init__(self,name, num_classes):
        """Generation function of the architecture

        Parameters
        ----------
        name: the name of the model, will have an impact on the name of the repertory in which our model will be saved
        num_classes : the number of classes for the classification

        Returns
        -------
        """
        super().__init__(name)
        self.c1 = torch.nn.Conv2d(in_channels=1, out_channels=6, kernel_size=5, stride=1, padding=0) # -> 24x24x6
        self.p1 = torch.nn.MaxPool2d(2, stride=2, padding=0) # -> 12x12x6
        self.c2 = torch.nn.Conv2d(in_channels=6, out_channels=16, kernel_size=5, stride=1, padding=0) # -> 8x8x16
        self.p2 = torch.nn.MaxPool2d(2, stride=2, padding=0) # -> 4x4x16
        self.l3 = torch.nn.Linear(256, 120) # 1x256 -> 1x120
        self.l4 = torch.nn.Linear(120, 84) # -> 1x84
        self.l5 = torch.nn.Linear(84, num_classes) # -> 1xnum_classes

    def forward(self, x):
        """The function processing the dataset throughout the NN

        Parameters
        ----------
        x: the dataset of images

        Returns
        -------
        x: the vector of size (1, nbr class) with each digit corresponding to a class' probability thanks to the softmax function

        """

        x = self.p1(F.relu(self.c1(x))) # image(x) -> conv1 -> relu -> pool -> x
        x = self.p2(F.relu(self.c2(x))) # x -> conv2 -> relu -> dropout -> x
        x = x.view(x.size(0), -1) # x -> vectorisation -> x
        x = F.relu(self.l3(x)) # x -> Lin -> x
        x = F.relu(self.l4(x))  # x -> Lin -> x
        x = self.l5(x)  # x -> Lin -> score de chaque classe -> x
        x = torch.nn.Softmax(1)(x) # x -> softmax -> proba de chaque classe
        return x


## Tests pour adapter Lenet5 à images d'entree RGB : resultats : en moy 50% - 65%

"""rgbLenet : CNN architecture LENET
Goal : Classification
Input of the NN : image with 3 channels (32x32*3)
Output of the NN : vector (1xnumber of classes) with each digit of the vector corresponding to a probability of matching which each class


mean results on CIFAR : env 55-65% 
"""
class rgbLenet(architecture):
    def __init__(self, name, num_classes):
        """Generation function of the architecture

        Parameters
        ----------
        name: the name of the model, will have an impact on the name of the repertory in which our model will be saved
        num_classes : the number of classes for the classification

        Returns
        -------
        """
        super().__init__(name)
        self.c0 = torch.nn.Conv2d(in_channels=3, out_channels=32, kernel_size=3, stride=1, padding=0)  # 32x32x3 -> 30x30x32
        self.d025 = torch.nn.Dropout(0.25)
        self.c1 = torch.nn.Conv2d(in_channels=32, out_channels= 32, kernel_size=3) #28
        self.p1 = torch.nn.MaxPool2d(2, stride=2, padding=0)  # -> 14x14x32
        self.cb = torch.nn.Conv2d(in_channels=32, out_channels=64, kernel_size=3, stride=1,padding=0)  # 12
        self.c2 = torch.nn.Conv2d(in_channels=64, out_channels=128, kernel_size=3, stride=1, padding=0)  # ->10
        self.p2 = torch.nn.MaxPool2d(2, stride=2, padding=0)  # -> 5x5x128
        self.l3 = torch.nn.Linear(3200, 120)  # 1x400 -> 1x120
        self.d05 = torch.nn.Dropout(0.5)
        self.l4 = torch.nn.Linear(120, 84)  # -> 1x84
        self.l5 = torch.nn.Linear(84, num_classes)  # -> 1xnum_classes

    def forward(self, x):
        """The function processing the dataset throughout the NN

        Parameters
        ----------
        x: the dataset of images

        Returns
        -------
        x: the vector of size (1, nbr class) with each digit corresponding to a class' probability thanks to the softmax function

        """
        x = self.d025(F.relu(self.c0(x)))
        x = self.p1(F.relu(self.c1(x))) # image(x) -> conv1 -> relu -> pool -> x
        x = self.d025(F.relu(self.cb(x)))
        x = self.p2(F.relu(self.c2(x))) # x -> conv2 -> relu -> pool -> x
        x = x.view(x.size(0), -1) # x -> vectorisation -> x
        x = self.d05(self.l3(x)) # x -> Lin -> x
        x = self.d05(self.l4(x))  # x -> Lin -> x
        x = self.l5(x)  # x -> Lin -> score de chaque classe -> x
        x = torch.nn.Softmax(1)(x) # x -> softmax -> proba de chaque classe
        return x