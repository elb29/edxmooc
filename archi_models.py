import torch
import torch.nn.functional as F

import  datetime, os, statistics
import numpy as np



"""
archi_models : classe mère des différentes architectures de réseaux de neurones
"""


class architecture(torch.nn.Module):

    def __init__(self,name):
        """Generation function of the architecture

                    Parameters
                    ----------
                    name: the name of the model, will have an impact on the name of the repertory in which our model will be saved

                    Returns
                    -------
        """

        super().__init__()
        self.name = name
        datetime_now = datetime.datetime.now()
        datetime_now = datetime_now.strftime("%Y_%m_%d_%H_%M_%S")
        print(datetime_now)
        self.dir = 'models/pytorch_' + self.name + '_' + str(datetime_now)
        os.makedirs(self.dir)

    def trainning(self, train_loader, epoch, no_epochs):
        """The function trainning the model for an epoch

            Parameters
            ----------
            train_loader: the trainning dataset
            epoch : number of the current epoch
            no_epochs : total numbers of epochs

            Returns
            -------
            self : the model with his parameters changed by the trainning
            accs : the list of accuracy of the whole epoch
            errs : the list of the loss fct results for the epoch
        """

        errs = []
        accs = []
        loss_func = torch.nn.CrossEntropyLoss()
        optimizer = torch.optim.Adam(self.parameters(), lr=1e-3)  # todo: investigate on optimizer
        # optimizer = torch.optim.SGD(model.parameters(),lr=1e-3) #pas bon pour RGB env. 9%

        # Train
        self.train()
        for i, (inputs, labels) in enumerate(train_loader):
            # Run the forward pass
            outputs = self.forward(inputs.float())
            input_labels = torch.tensor(labels, dtype=torch.long)
            loss = loss_func(outputs, input_labels)
            errs.append(loss.cpu().detach().item())

            # Backprop and perform optimisation
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            # Track the accuracy
            total = input_labels.size(0)
            _, predicted = torch.max(outputs.data, 1)
            correct = (predicted == input_labels).sum().item()
            accs.append(correct / total)

            if (i + 1) % len(train_loader) == 0:
                print('Epoch [{}/{}], Step [{}/{}], Loss: {:.4f}, Accuracy: {:.2f}%'
                      .format(epoch + 1, no_epochs, i + 1, len(train_loader), loss.item(),
                              (correct / total) * 100))
                # writer.add_scalar('Train/Loss', np.mean(errs), epoch)
                # writer.add_scalar('Train/Acc', np.mean(accs), epoch)

        # Save the model
        torch.save({'epoch': epoch,
                    'model_state_dict': self.state_dict(),
                    'optimizer_state_dict': optimizer.state_dict(),
                    'loss': loss,
                    }, self.dir + '/conv_net_model_' + str(epoch) + '_' + str(
            np.mean(accs)) + '.ckpt')

        return self, accs, errs

    def validate(self, val_loader, epoch, no_epochs):
        """Testing the model with a dataset

            Parameters
            ----------
            val_loader: the validating/testing dataset
            epoch : number of the current epoch
            no_epochs : total numbers of epochs

            Returns
            -------
            accs : the list of accuracy of the whole epoch
            errs : the list of the loss fct results for the epoch
        """
        errs = []
        accs = []
        loss_func = torch.nn.CrossEntropyLoss()

        self.eval()
        for i, (images, labels) in enumerate(val_loader):

            # Run the forward pass
            outputs = self.forward(images.float())
            labels = torch.tensor(labels, dtype=torch.long)
            loss = loss_func(outputs, labels)
            errs.append(loss.cpu().detach().item())

            # Track the accuracy
            total = labels.size(0)
            _, predicted = torch.max(outputs.data, 1)
            correct = (predicted == labels).sum().item()
            accs.append(correct / total)

            if (i + 1) % len(val_loader) == 0:
                print('Epoch [{}/{}], Step [{}/{}], Mean accuracy of the epoch: {:.2f}%\n'
                      .format(epoch + 1, no_epochs, i + 1, len(val_loader), statistics.mean(accs) * 100))
                niter = epoch * len(val_loader) + i
                # writer.add_scalar('Validation/Loss', np.mean(errs), epoch)
                # writer.add_scalar('Validation/Acc', np.mean(accs), epoch)

        return accs, errs


"""Pytorch_CNN : Basic CNN architecture
Goal : Classification
Input of the NN : image with 1 channel (28x28)
Output of the NN : vector (1xnumber of classes) with each digit of the vector corresponding to a score of matching which each class 

mean accuracy on MNIST : +90 %
"""

class pytorch_cnn(architecture):
    def __init__(self, name, num_classes):
        """Generation function of the architecture

        Parameters
        ----------
        name: the name of the model, will have an impact on the name of the repertory in which our model will be saved
        num_classes : the number of classes for the classification

        Returns
        -------
        """
        super().__init__(name)
        self.c1 = torch.nn.Conv2d(in_channels=1, out_channels=32, kernel_size=3, stride=1, padding=0) # une image en nuances de gris, 32 neurones, un filtre de taille 3x3, decalage 1 pix, rajout de pix 0 -> matrice de sortie ((nPixels-3+1=26)x(nPixels-3+1))x32
        self.p1 = torch.nn.MaxPool2d(2, stride=2, padding=0) # matrice de sortie c1 (26x26)x32 -> max des matrices (fxf) décalées de stride ( sortie : (n+2p-f)/s  + 1) -> (13x13)x32
        self.c2 = torch.nn.Conv2d(in_channels=32, out_channels=64, kernel_size=3) # sorties des 32 neurones précedents, 64 neurones, un filtre de taille 3x3 -> matrice de sortie (11x11)x64
        self.d2 = torch.nn.Dropout(0.2) # matrice de sortie c2 (11x11)x64 -> (11x11)x64
        self.l3 = torch.nn.Linear(7744, 32) # 11 * 11 * 64 = 7744
        self.l4 = torch.nn.Linear(32, num_classes) # fully connected

    def forward(self, x):
        """The function processing the dataset throughout the NN

        Parameters
        ----------
        x: the dataset of images

        Returns
        -------
        x: the vector of size (1, nbr class) with each digit corresponding to a class' score

        """
        x = self.p1(F.relu(self.c1(x))) # image(x) -> conv1 -> relu -> pool -> x
        x = self.d2(F.relu(self.c2(x))) # x -> conv2 -> relu -> dropout -> x
        x = x.view(x.size(0), -1) # x -> vectorisation -> x
        x = F.relu(self.l3(x)) # x -> Lin -> x
        x = self.l4(x)  # x -> Lin -> score de chaque classe
        return x
