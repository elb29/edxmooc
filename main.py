import torchvision
import gzip, struct, random
import matplotlib.pylab as plt
import PIL.Image as Image

"""
Main : loading data and launching the training of a model
"""

from scr.unet import *
from scr.lenet import *

# import archi VGG16 de torchvision
def creationVGG16(name="vgg16"):
    """Loading the architecture VGG16 from torchvision

       Parameters
       ----------
       name: the name of the model, will have an impact on the name of the repertory in which our model will be saved

       Returns
       -------
    """
    model = torchvision.models.vgg16()
    ghostM = architecture(name)
    model.dir = ghostM.dir
    return(model)


### FUNCTIONS ###

def read_idx(filename):
    """read zipped image files

       Parameters
       ----------
       filename: the file path

       Returns
       -------
       the tuple image
    """
    with gzip.open(filename, 'rb') as f:
        zero, data_type, dims = struct.unpack('>HBB', f.read(4))
        shape = tuple(struct.unpack('>I', f.read(4))[0] for d in range(dims))
        return np.fromstring(f.read(), dtype=np.uint8).reshape(shape)

def importLocalDatasetSegmentation(repTrain, repTest, repLabTrain, repLabTest):  # trigger erreur code 137 : cpu pas suffisant à tester sur machine gpu
    """Import the dataset for segmentation (postdam)

       Parameters
       ----------
       repTrain : path of the trainning dataset
       repTest  : path of the test dataset
       repLabTrain : path of the training labels dataset
       repLabTest  : path of the test label dataset

       Returns
       -------
       trainset : training dataset
       testset : test dataset
    """
    repertories = [repTrain, repTest, repLabTrain, repLabTest]

    files = []

    for repertory in repertories:
        files.append([os.path.join(repertory, f) for f in os.listdir(repertory) if os.path.isfile(os.path.join(repertory, f))])

    tensors = []

    for fs in files:
        tensors.append(torch.stack([torch.from_numpy(np.array(Image.open(f))/255) for f in fs]))

    trainset = torch.utils.data.TensorDataset(tensors[0], tensors[2])
    testset = torch.utils.data.TensorDataset(tensors[1], tensors[4])

    return(trainset,testset)

def loadCIFAR():
    """Import the CIFAR dataset from torchvision

           Parameters
           ----------

           Returns
           -------
           trainset : training dataset
           testset : test dataset
    """
    transform = torchvision.transforms.Compose(
        [torchvision.transforms.ToTensor(),
         torchvision.transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

    trainset = torchvision.datasets.CIFAR10(root='./data', train=True,
                                            download=True, transform=transform)
    testset = torchvision.datasets.CIFAR10(root='./data', train=False,
                                           download=True, transform=transform)


    return trainset, testset

def loadLocalMNIST(trainimpath,trainlabpath,testimpath,testlabpath):
   """Import the MNIST dataset from local

               Parameters
               ----------
               trainimpath  : path of train dataset
               trainlabpath : path of train labels dataset
               testimpath   : path of test datatset
               testlabpath  : path of test labels dataset

               Returns
               -------
               trainset : training dataset
               testset  : test dataset
   """
   train_x = read_idx(trainimpath)
   train_y = read_idx(trainlabpath)
   test_x = read_idx(testimpath)
   test_y = read_idx(testlabpath)

   ##Normalise the data
   train_x = train_x / 255
   test_x = test_x / 255

   #### PRE PROCESSING THE DATA ####
   input_shape = (1, 28, 28)  # 1 channel, x= 28, y = 28

   x_train = train_x.reshape(train_x.shape[0], *input_shape)
   x_test = test_x.reshape(test_x.shape[0], *input_shape)

   x_train = torch.stack([torch.from_numpy(np.array(i)) for i in x_train])
   x_test = torch.stack([torch.from_numpy(np.array(i)) for i in x_test])
   y_train = torch.stack([torch.from_numpy(np.array(i)) for i in train_y])
   y_test = torch.stack([torch.from_numpy(np.array(i)) for i in test_y])

   trainset = torch.utils.data.TensorDataset(x_train, y_train)
   testset = torch.utils.data.TensorDataset(x_test, y_test)

   return trainset, testset

def plotValidationResults(loss,accu):
    """Plot the evolution of loss and accuracy during the whole training phase

      Parameters
      ----------
      loss: losses list
      accu : accuracy list

      Returns
      -------
      """

    plt.figure()

    plt.subplot(211)
    plt.plot(accu)
    plt.ylabel('validation accuracy')

    plt.subplot(212)
    plt.plot(loss)
    plt.ylabel('validation loss')

    plt.xlabel('epoch')

    plt.show()



if __name__ == '__main__':
    print("Starting at :")

    batch_size = 512
    no_epochs = 15

    #target_names = ['top', 'trouser', 'pullover', 'dress', 'coat', 'sandal', 'shirt', 'sneaker', 'bag', 'boot']
    target_names = ['plane', 'car', 'bird', 'cat','deer', 'dog', 'frog', 'horse', 'ship', 'truck']
    num_classes = len(target_names)

#### LOADING DATA ####

    trainset, testset = loadCIFAR()
    #trainset, testset = loadLocalMNIST('/home/ign.fr/ele-bihan/Images/DL/train-images.gz','/home/ign.fr/ele-bihan/Images/DL/train-labels.gz','/home/ign.fr/ele-bihan/Images/DL/t10k-images.gz','/home/ign.fr/ele-bihan/Images/DL/t10k-labels.gz')
    #trainset, testset = importLocalDatasetSegmentation()

#### BUILDING THE TRAIN VALIDATION TEST SETS ####

    trainset, valset = torch.utils.data.random_split(trainset,
                                                     [round(len(trainset) * 0.75), round(len(trainset) * 0.25)])

    train_loader = torch.utils.data.DataLoader(trainset, batch_size=batch_size, shuffle=True, num_workers=0)
    val_loader = torch.utils.data.DataLoader(valset, batch_size=batch_size, shuffle=True, num_workers=0)
    test_loader = torch.utils.data.DataLoader(testset, batch_size=batch_size, shuffle=False, num_workers=0)


#### CREATE NETWORK ####

    #model = pytorch_cnn(name="CNNexemple", num_classes=num_classes)

    #model = leNet5(name="LeNet-5",num_classes=num_classes)

    model = rgbLenet(name="RGB_LENET5", num_classes=num_classes)

    #model = creationVGG16()

    #model = torchvision.models.vgg16(pretrained=True) #pretrained vgg16

    #model = UNet(3, num_classes)

#### TRAINING ####

    tr_err = []
    tr_acc = []
    ts_err = []
    ts_acc = []

    for epoch in range(no_epochs):  # loop over the dataset multiple times

        model, accs, errs = model.trainning(train_loader, epoch, no_epochs)

        tr_err.append(np.mean(errs))
        tr_acc.append(np.mean(accs))

        accs, errs = model.validate(val_loader, epoch, no_epochs)

        ts_err.append(np.mean(errs))
        ts_acc.append(np.mean(accs))

        history_values = []
        history_values.append(tr_err)
        history_values.append(tr_acc)
        history_values.append(ts_err)
        history_values.append(ts_acc)

    plotValidationResults(ts_err,ts_acc)

    print('Finished Training')

    print('Testing the model :')
    model.validate(test_loader,0,1)
    #print(model.state_dict())


